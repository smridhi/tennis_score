class TennisMatch:
    points = {0: 0, 1: 15, 2: 30, 3: 40, 4: 50}

    def __init__(self):
        self.A_sets = 0
        self.B_sets = 0
        self.reset_game()

    def reset_game(self):
        self.A_pts = 0
        self.B_points = 0
        self.A_games = 0
        self.B_games = 0

    def point_won(self, player):
        if player == 'A':
            self.A_pts += 1
        else:
            self.B_points += 1

        self.game_winner()

    def game_winner(self):
        if self.A_pts >= 4 and self.A_pts >= self.B_points + 2:
            self.A_games += 1
            self.reset_points()
        elif self.B_points >= 4 and self.B_points >= self.A_pts + 2:
            self.B_games += 1
            self.reset_points()

        self.set_winner()

    def reset_points(self):
        self.A_pts = 0
        self.B_points = 0

    def set_winner(self):
        if self.A_games >= 6 and self.A_games >= self.B_games + 2:
            self.A_sets += 1
            self.reset_games()
        elif self.B_games >= 6 and self.B_games >= self.A_games + 2:
            self.B_sets += 1
            self.reset_games()

        self.match_winner()

    def reset_games(self):
        self.A_games = 0
        self.B_games = 0

    def match_winner(self):
        if self.A_sets >= 3:
            print("Player 1 wins the match")
            self.print_score()
        elif self.B_sets >= 3:
            print("Player 2 wins the match")
            self.print_score()

    def print_score(self):
        A_point_display = self.points.get(self.A_pts, 50)
        B_point_display = self.points.get(self.B_points, 50)
        
        print(f"{A_point_display} {B_point_display}")

    def play_match(self, sequence):
        for point in sequence:
            self.point_won(point)
            self.print_score()
        if self.A_sets < 3 and self.B_sets < 3:
            print("Incomplete")

match_sequence = "ABABBBBABABABBB"
match = TennisMatch()
match.play_match(match_sequence)

